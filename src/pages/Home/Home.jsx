import React from 'react'

import styles from './Home.module.scss'

const Home = ({ gameFinished, setGameStarted, reward }) => {
  return (
    <section className={styles.root}>
      <img src="/images/like.png" alt="like" className={styles.image} />

      <div className={styles.content}>
        {gameFinished ? (
          <>
            <p className={styles.score}>Total Score:</p>
            <h2 className={styles.title}>{`${reward} earned`}</h2>
          </>
        ) : (
          <h1 className={styles.title}>Who wants to be a millionaire?</h1>
        )}

        <button
          type="button"
          className={styles.button}
          onClick={() => setGameStarted(true)}
        >
          {gameFinished ? 'Try Again' : 'Start'}
        </button>
      </div>
    </section>
  )
}

export default Home
