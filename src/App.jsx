import React, { useState } from 'react'

import Game from './pages/Game/Game'
import Home from './pages/Home/Home'

const App = () => {
  const [gameStarted, setGameStarted] = useState(false)
  const [gameFinished, setGameFinished] = useState(false)
  const [reward, setReward] = useState('')

  return (
    <main>
      {gameStarted ? (
        <Game
          setGameFinished={setGameFinished}
          setReward={setReward}
          setGameStarted={setGameStarted}
        />
      ) : (
        <Home
          gameFinished={gameFinished}
          reward={reward}
          setGameStarted={setGameStarted}
        />
      )}
    </main>
  )
}

export default App
